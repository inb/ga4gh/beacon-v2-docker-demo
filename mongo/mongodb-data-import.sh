#! /bin/bash

mongoimport --uri="mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --collection=Analyses --type=json --jsonArray --file <(xz -d -c -q -q /beacon-data/analyses.json.lzma)
mongoimport --uri="mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --collection=Biosamples --type=json --jsonArray --file <(xz -d -c -q -q /beacon-data/biosamples.json.lzma)
mongoimport --uri="mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --collection=Cohorts --type=json --jsonArray --file <(xz -d -c -q -q /beacon-data/cohorts.json.lzma)
mongoimport --uri="mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --collection=Datasets --type=json --jsonArray --file <(xz -d -c -q -q /beacon-data/datasets.json.lzma)

mongoimport --uri="mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --collection=Variants --type=json --jsonArray --file <(xz -d -c -q -q /beacon-data/genomicVariationsVcf.json.lzma)

mongoimport --uri="mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --collection=Individuals --type=json --jsonArray --file <(xz -d -c -q -q /beacon-data/individuals.json.lzma)
mongoimport --uri="mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --collection=Runs --type=json --jsonArray --file <(xz -d -c -q -q /beacon-data/runs.json.lzma)

mongoimport --uri="mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --collection=Individuals --type=json --jsonArray --file <(xz -d -c -q -q /beacon-data/omop-individuals.json.xz)
mongoimport --uri="mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --collection=Biosamples --type=json --jsonArray --file <(xz -d -c -q -q /beacon-data/omop-biosamples.json.xz)
mongoimport --uri="mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --collection=Variants --type=json --jsonArray --file <(xz -d -c -q -q /beacon-data/omop-genomicVariants.json.xz)

#mongoimport --uri="mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --collection=Individuals --type=json --jsonArray --file <(xz -d -c -q -q /beacon-data/usecase-1.patients.json.lzma)
#mongoimport --uri="mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --collection=Variants --type=json --jsonArray --file <(xz -d -c -q -q /beacon-data/cbioportal-blca_mskcc_solit_2014-genomicVariants.json.xz)
#mongoimport --uri="mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --collection=Variants --type=json --jsonArray --file <(xz -d -c -q -q /beacon-data/usecase-1.variants.json.lzma)

mongo "mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --eval "db.Analyses.createIndex({'id': 1}, {name: 'id', unique: true, background: true})"
mongo "mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --eval "db.Biosamples.createIndex({'id': 1}, {name: 'id', unique: true, background: true})"

mongo "mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --eval "db.Variants.createIndex({'id': 1}, {name: 'id', unique: true, background: true})"
mongo "mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --eval "db.Variants.createIndex({'molecularAtrributes.geneIds': 1}, {name: 'geneId', background: true})"
mongo "mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --eval "db.Variants.createIndex({'variantInternalId': 1}, {name: 'variantInternalId', background: true})"
mongo "mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --eval "db.Variants.createIndex({'molecularAttributes.aminoacidChanges': 1}, {name: 'aaChanges', background: true})"
mongo "mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --eval "db.Variants.createIndex({'position.refseqId': 1}, {name: 'referenceName', background: true})"
mongo "mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --eval "db.Variants.createIndex({'caseLevelData.analysisId': 1}, {name: 'analysisId', background: true})"
mongo "mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --eval "db.Variants.createIndex({'caseLevelData.biosampleId': 1}, {name: 'biosampleId', background: true})"
mongo "mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --eval "db.Variants.createIndex({'caseLevelData.individualId': 1}, {name: 'individualId', background: true})"
mongo "mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --eval "db.Variants.createIndex({'caseLevelData.runId': 1}, {name: 'runId', background: true})"

mongo "mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --eval "db.Individuals.createIndex({'id': 1}, {name: 'id', unique: true, background: true})"
mongo "mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --eval "db.Individuals.createIndex({'sex.id': 1}, {name: 'sex', unique: false, background: true})"
mongo "mongodb://admin:admin@localhost:27017/beacon?authSource=admin" --eval "db.Runs.createIndex({'id': 1}, {name: 'id', unique: true, background: true})"
