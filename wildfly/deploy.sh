#! /bin/bash

# download beacon server for the deployment
curl https://inb.bsc.es/maven/es/bsc/inb/ga4gh/beacon-nosql-server/2.0.5/beacon-nosql-server-2.0.5.war > /opt/jboss/wildfly/standalone/deployments/beacon-nosql-server-2.0.5.war

/opt/jboss/wildfly/bin/standalone.sh -b 0.0.0.0